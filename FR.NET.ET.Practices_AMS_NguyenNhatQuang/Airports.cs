namespace AirportManagementSystem;

public class Airports
{
    public string Id { get; set; }
    public string Name { get; set; }
    public double RunwaySize { get; set; }
    public int MaxFixedwingParkingPlace { get; set; }
    public List<Fixedwings> listOfFixedwings { get; } = new List<Fixedwings>();
    public int MaxRotatedwingParkingPlace { get; set; }
    public List<Helicopters> listOfHelicopters { get; } = new List<Helicopters>();
    private List<Airports> ListOfAirport { get; } = new List<Airports>();

    public void CreateNewAirport(string name, double runwaySize)
    {
        ListOfAirport.Add(new Airports
        {
            Name = name,
            RunwaySize = runwaySize
        });
    }

    public void DeletingAirport(string id)
    {
        var removedAirport = ListOfAirport.Find(entry => entry.Id.Contains(id));
        if (removedAirport is null)
        {
            throw new Exception("This air ports invalid");
        }
        else
        {
            ListOfAirport.Remove(removedAirport);
            Console.WriteLine("Deleting Air Port is successfully");
        }
    }

    public void AddingFixedWingAirPlanesToAirports(Fixedwings fixedwings)
    {
        if(fixedwings.MinNeededRunwaySize < RunwaySize){
            listOfFixedwings.Add(fixedwings);
        }
        else
        {
            throw new Exception("The runway size of fixed wings must shorter than runway of Airport");
        }
    }

    public void AddingHelicopterToAirport(Helicopters helicopters)
    {
        
        var helicoptersIdInList = listOfHelicopters.Find(entry => entry.Id.Contains(helicopters.Id)) ?? throw new ArgumentNullException("listOfHelicopters.Find(entry => entry.Id == helicopters.Id)");
        if (helicoptersIdInList is null)
        {
            listOfHelicopters.Add(helicopters);
        }
    }
    
    

}