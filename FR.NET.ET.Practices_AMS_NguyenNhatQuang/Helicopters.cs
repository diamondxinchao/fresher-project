namespace AirportManagementSystem;

public class Helicopters : Airplane
{
    public double Range { get; set; }

    public override string Fly()
    {
        return "rotated wing";
    }
}