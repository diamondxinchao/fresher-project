﻿using AirportManagementSystem;

var airportManagement = new Airports();
int choice = 0;

do
{
    try
    {
        GetMenu();
        choice = GetChoice();
        switch (choice)
        {
            case 1:
                CreateNewAirPort();
                break;
            case 2:
                DeleteAirport();
                break;
            case 3:
                break;
            case 4:
                break;
            case 5:
                break;
            case 6:
                break;
            case 7:
                break;
            default:
                Console.WriteLine("Thank for using my service");
                break;
        }
    }
    catch (Exception e)
    {
        Console.WriteLine(e.Message);
        throw;
    }
} while (choice != 7);

void CreateNewAirPort()
{
    Console.Write("Enter name of AirPort: ");
    string airportName = Console.ReadLine()!;
    Console.Write("Enter runway size: ");
    double runwaySize = double.Parse(Console.ReadLine()!);
    airportManagement.CreateNewAirport(airportName, runwaySize);
}

void DeleteAirport()
{
    Console.Write("Enter AirPort Id to delete: ");
    string airportId = Console.ReadLine()!;
    airportManagement.DeletingAirport(airportId);
}

void AddFixedWingsToAirport()
{
    Console.WriteLine("Enter fixed wing id: ");
    var id = Console.ReadLine()!;
    Console.WriteLine("Enter fixed model: ");
    var model = Console.ReadLine()!;
    Console.WriteLine("Enter plane type of fixed plane: ");
    var planeType = Console.ReadLine()!;
    Console.WriteLine("Enter cruise speed: ");
    var cruiseSpeed = double.Parse(Console.ReadLine()!);
    Console.WriteLine("Enter entry weight of fixed wings");
    var emptyWeight = double.Parse(Console.ReadLine()!);
    Console.WriteLine("Enter max take off Weight: ");
    var maxTakeOffWeight = double.Parse(Console.ReadLine()!);
    Console.WriteLine("Enter min needed runway size: ");
    var minRunwaySize = double.Parse(Console.ReadLine()!);
    // var fixedWings = new Fixedwings(id, model, planeType, cruiseSpeed, emptyWeight, maxTakeOffWeight, minRunwaySize);
}

int GetChoice()
{
    try
    {
        Console.WriteLine("Enter your choice: ");
        int choice = int.Parse(Console.ReadLine()!);
        return choice;
    }
    catch (Exception e)
    {
        Console.WriteLine("Wrong format, Please enter again");
        throw;
    }
}

void GetMenu()
{
    Console.WriteLine("1. Create Airport");
    Console.WriteLine("2. Delete Airport");
    Console.WriteLine("3. Add Fixed-Wing Airplane to Airport");
    Console.WriteLine("4. Remove Helicopters from Airport");
    Console.WriteLine("5. Add Helicopters to Airport");
    Console.WriteLine("6. Change Plane Type and Runway Size for Fixed-Wing Airplane");
    Console.WriteLine("7. Exit");
}