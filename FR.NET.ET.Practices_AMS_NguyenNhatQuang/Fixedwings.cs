namespace AirportManagementSystem;

public class Fixedwings : Airplane
{
    
    public double MinNeededRunwaySize { get; set; }

    public override string Fly()
    {
        return "fixed wings";
    }
}