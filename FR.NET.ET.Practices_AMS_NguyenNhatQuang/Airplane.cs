using System.Security.AccessControl;
using System.Xml.Schema;

namespace AirportManagementSystem;

public abstract class Airplane
{
    public string Id { get; set; }
    public string Model { get; set; }
    public double CruiseSpeed { get; set; }
    public double EmptyWeight { get; set; }
    public double MaxTakeOffWeight { get; set; }

    public Airplane()
    {

    }


    public abstract string Fly();
}